package org.rogueware.configuration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;
import org.junit.Before;
import org.junit.Test;

public class TestEnvSystemValueInputStream {
    
    @Before
    public void initProps() {
        System.setProperty("AUTHOR", "Mr Test me!!");
        System.setProperty("THE_DIR", "/var/dir/me");
        System.setProperty("QOS", "99");
        System.setProperty("LRMS_PASSWORD", "lrms_passme");
        System.setProperty("TRACKING_PASSWORD", "track_passme");
    }

    @Test
    public void testStream() throws IOException {        
        InputStream is = new EnvSystemValueInputStream(new FileInputStream("src/test/resources/Sample.xml"));

        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line;
        while (null != (line = br.readLine())) {
            sb.append(line);
            sb.append("\n");
        }

        String txt = sb.toString();
        br.close();        

        assertTrue(txt.contains("Author     : Mr Test me!!"));
        assertTrue(txt.contains("<WorkingArea dir=\"/var/dir/me\"/>"));
        assertTrue(txt.contains("password=\"lrms_passme\""));
        assertTrue(txt.contains("password=\"track_passme\""));
        assertTrue(txt.contains("qos=\"99\"/>"));
        assertTrue(txt.contains("qos=\"99\" subscribe=\"false\"/>"));

        assertTrue(txt.contains("<Test dir=\"${MISSING}\"/>"));        
    }


    @Test
    public void testXmlLoaf() throws IOException, ConfigurationException {
         InputStream is = new EnvSystemValueInputStream(new FileInputStream("src/test/resources/Sample.xml"));
         XMLConfiguration config = new XMLConfiguration();
         config.load(is);
         config.setThrowExceptionOnMissing(true);        

         String dir = config.getString(".WorkingArea[@dir]");    
         assertEquals("/var/dir/me", dir);         
    }
}
