package org.rogueware.configuration;

import java.util.concurrent.TimeUnit;

/**
 *
 * @author Tony Abbott
 */
public class TimeDuration {

   private TimeDuration() {
      // do not instantiate, only static methods here
   }

   /**
    * Format a time duration in a human-readable format. Uses the format
    * <em>6d 23h 59m 59.999s</em>. For example
    * <em>3h 0m 56.500s</em> or <em>5m 36s</em>.
    * <p>
    * Units are:
    * <ul>
    * <li>Days (d)
    * <li>Hours (h)
    * <li>Minutes (m)
    * <li>Seconds (s)
    * <li>Milliseconds (expressed as decimal places of seconds)
    * </ul>
    *
    * @param duration duration in the specified units
    * @param unit the time unit for the duration
    *
    * @return human readable duration formatted string
    */
   public static String toString(long duration, TimeUnit unit) {
      final long millis = TimeUnit.MILLISECONDS.convert(duration, unit);

      final StringBuilder sb = new StringBuilder();
      final long days = millis / 86400000;
      final long hours = (millis / 3600000) % 24;
      final long minutes = (millis / 60000) % 60;
      final long seconds = (millis / 1000) % 60;
      if (days > 0) {
         sb.append(days);
         sb.append("d ");
      }
      if (hours > 0 || days > 0) {
         sb.append(hours);
         sb.append("h ");
      }
      if (minutes > 0 || hours > 0 || days > 0) {
         sb.append(minutes);
         sb.append("m ");
      }
      if (0 == millis % 1000) {
         sb.append(seconds);
         sb.append('s');
      } else {
         sb.append(String.format("%d.%03ds", seconds, millis % 1000));
      }
      return sb.toString();
   }

   /**
    * Get a numeric time duration from a human-readable string. Uses the format
    * <em>6d 23h 59m 59.999s</em>. If any value is 0, that term may be omitted.
    * For example
    * <em>3h 56.500s</em> or <em>5m 36s</em>.
    * <p>
    * Units are:
    * <ul>
    * <li>Days (d)
    * <li>Hours (h)
    * <li>Minutes (m)
    * <li>Seconds (s)
    * <li>Milliseconds (expressed as decimal places of seconds)
    * </ul>
    *
    * @param duration the duration string
    * @param unit the units to return
    *
    * @return the duration in the specified units
    *
    * @throws IllegalArgumentException if the duration format is invalid
    */
   public static long fromString(String duration, TimeUnit unit) {
      long lastUnitInMillis = Long.MAX_VALUE;
      long result = 0;
      int startPos = 0;
      while (startPos < duration.length() && Character.isWhitespace(duration.charAt(startPos))) {
         ++startPos;
      }
      while (startPos < duration.length()) {
         int unitPos = startPos;
         while (unitPos < duration.length() && (Character.isDigit(duration.charAt(unitPos)) || '.' == duration.charAt(unitPos))) {
            ++unitPos;
         }
         if (unitPos >= duration.length()) {
            throw new IllegalArgumentException("Missing unit at position " + unitPos);
         }
         if (startPos == unitPos) {
            throw new IllegalArgumentException("Missing digits at position " + unitPos);
         }
         switch (duration.charAt(unitPos)) {
            case 'D':
            case 'd': {
               final long myUnitInMillis = TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS);
               if (myUnitInMillis >= lastUnitInMillis) {
                  throw new IllegalArgumentException("Invalid unit sequence");
               }
               lastUnitInMillis = myUnitInMillis;

               final long days = Long.valueOf(duration.substring(startPos, unitPos));
               result += unit.convert(days, TimeUnit.DAYS);
               break;
            }

            case 'H':
            case 'h': {
               final long myUnitInMillis = TimeUnit.MILLISECONDS.convert(1, TimeUnit.HOURS);
               if (myUnitInMillis >= lastUnitInMillis) {
                  throw new IllegalArgumentException("Invalid unit sequence");
               }
               lastUnitInMillis = myUnitInMillis;

               final long hours = Long.valueOf(duration.substring(startPos, unitPos));
               if (hours > 23) {
                  throw new IllegalArgumentException("Invalid hours value");
               }
               result += unit.convert(hours, TimeUnit.HOURS);
               break;
            }

            case 'M':
            case 'm': {
               final long myUnitInMillis = TimeUnit.MILLISECONDS.convert(1, TimeUnit.MINUTES);
               if (myUnitInMillis >= lastUnitInMillis) {
                  throw new IllegalArgumentException("Invalid unit sequence");
               }
               lastUnitInMillis = myUnitInMillis;

               final long mins = Long.valueOf(duration.substring(startPos, unitPos));
               if (mins > 59) {
                  throw new IllegalArgumentException("Invalid minutes value");
               }
               result += unit.convert(mins, TimeUnit.MINUTES);
               break;
            }

            case 'S':
            case 's': {
               final long myUnitInMillis = TimeUnit.MILLISECONDS.convert(1, TimeUnit.SECONDS);
               if (myUnitInMillis >= lastUnitInMillis) {
                  throw new IllegalArgumentException("Invalid unit sequence");
               }
               lastUnitInMillis = myUnitInMillis;

               final String vs = duration.substring(startPos, unitPos);
               final int decimalPos = vs.indexOf('.');
               if (-1 == decimalPos) {
                  final long secs = Long.valueOf(vs);
                  if (secs > 59) {
                     throw new IllegalArgumentException("Invalid seconds value");
                  }
                  result += unit.convert(secs, TimeUnit.SECONDS);
               } else {
                  final long secs = Long.valueOf(vs.substring(0, decimalPos));
                  String milliString = vs.substring(decimalPos + 1);
                  while (milliString.length() < 3) {
                     milliString += "0";
                  }
                  final long millis = Long.valueOf(milliString);
                  if (secs > 59) {
                     throw new IllegalArgumentException("Invalid seconds value");
                  }
                  if (millis > 999) {
                     throw new IllegalArgumentException("Invalid milliseconds value");
                  }
                  result += unit.convert(secs, TimeUnit.SECONDS);
                  result += unit.convert(millis, TimeUnit.MILLISECONDS);
               }
               break;
            }

            default:
               throw new IllegalArgumentException("Invalid unit " + duration.charAt(unitPos));
         }
         startPos = unitPos + 1;
         while (startPos < duration.length() && Character.isWhitespace(duration.charAt(startPos))) {
            ++startPos;
         }
      }
      return result;
   }
}
