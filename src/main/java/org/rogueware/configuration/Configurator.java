/*
 * Copyright 2017 jingram.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.rogueware.configuration;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.event.Event;
import jakarta.enterprise.inject.Produces;
import jakarta.enterprise.inject.spi.InjectionPoint;
import jakarta.inject.Inject;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.text.SimpleDateFormat;
import org.rogueware.configuration.qualifier.Configuration;
import org.rogueware.configuration.qualifier.ConfigurationFile;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import org.apache.commons.configuration.XMLConfiguration;
import org.rogueware.configuration.delegate.CBoolean;
import org.rogueware.configuration.delegate.CDouble;
import org.rogueware.configuration.delegate.CFloat;
import org.rogueware.configuration.delegate.CInteger;
import org.rogueware.configuration.delegate.CList;
import org.rogueware.configuration.delegate.CLong;
import org.rogueware.configuration.delegate.CMap;
import org.rogueware.configuration.delegate.CString;
import org.rogueware.configuration.delegate.CStructure;
import org.rogueware.configuration.delegate.CTimeDuration;
import org.rogueware.configuration.delegate.ConfigurationValue;
import org.rogueware.configuration.qualifier.ConfigurationList;
import org.rogueware.configuration.qualifier.ConfigurationMap;
import org.rogueware.configuration.qualifier.ConfigurationRoot;
import org.rogueware.configuration.qualifier.ConfigurationScheduledExecutorService;
import org.rogueware.configuration.qualifier.ConfigurationStructure;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@ApplicationScoped
public class Configurator implements Runnable {

   public static final String DEFAULT = "[_{!}_]";
   private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

   private XMLConfiguration config;
   private long parsedLastModified;
   private long loadedLastModified;
   private ScheduledFuture<?> pollFuture = null;

   private final Map<String, Object> configurationValues = Collections.synchronizedMap(new HashMap<>());  // Config element, val
   private final Map<String, ConfigurationValue> delegates = Collections.synchronizedMap(new HashMap<>());  // Config element, delegate

   @Inject
   @ConfigurationFile
   String configFile;

   @Inject
   @ConfigurationRoot
   String configurationRoot;

   @Inject
   @ConfigurationScheduledExecutorService
   ScheduledExecutorService ses;

   @Inject
   Event<ConfigurationChanged> configurationChangedEvent;

   @PostConstruct
   public void init() {
      readConfigurationFile();

      // If we have a scheduled executor, schedule a task for polling for new config reloads 
      if (ses != null) {
         try {
            pollFuture = ses.scheduleWithFixedDelay(this, 120, 30, TimeUnit.SECONDS);
         } catch (Exception ex) {
            throw new IllegalArgumentException("Unable to schedule configuration file change monitoring task");
         }
      }
   }

   @PreDestroy
   public void shutdown() {
      // Stop task polling if running
      if (null != pollFuture) {
         try {
            pollFuture.cancel(true);
            pollFuture = null;
         } catch (Throwable ex) {
         }
      }
      delegates.clear();
      configurationValues.clear();
   }

   @Produces
   @ConfigurationMap(element = "", subElementMapping = "", mapKeyAttribute = "")
   public <M extends Map> CMap<M> produceMapDelegate(InjectionPoint injectionPoint) {
      ConfigurationMap annotation = (ConfigurationMap) injectionPoint.getQualifiers().stream().filter(a -> a.annotationType().equals(ConfigurationMap.class)).findFirst().orElse(null);
      String target = (null == injectionPoint.getBean() ? "Dynamic lookup" : injectionPoint.getMember().getDeclaringClass().getSimpleName() + "." + injectionPoint.getMember().getName());

      if (null != annotation) {
         if (null != annotation.element() && !annotation.element().isEmpty()
                 && null != annotation.subElementMapping() && !annotation.subElementMapping().isEmpty()
                 && null != annotation.mapKeyAttribute() && !annotation.mapKeyAttribute().isEmpty()) {
            String expandedKey = expandKey(annotation.element());

            // Add the sub-element mapping
            expandedKey = expandedKey + "." + annotation.subElementMapping();

            CMap<M> map = (CMap<M>) delegates.get(expandedKey);
            if (null == map) {
               try {
                  if (null != injectionPoint.getMember()) {
                     // Injected
                     Field f = injectionPoint.getMember().getDeclaringClass().getDeclaredField(injectionPoint.getMember().getName());
                     map = new CMap<>(expandedKey, annotation.mapKeyAttribute(), annotation.mapValueAttribute(), f, config, configurationValues);
                  } else {
                     // Literal
                     // Example Usage: See ConfigurationStructureLiteral.java                     
                     ParameterizedType type = (ParameterizedType)injectionPoint.getType();
                     map = new CMap<>(expandedKey, annotation.mapKeyAttribute(), annotation.mapValueAttribute(), type, config, configurationValues);
                  }                                    
                  delegates.put(expandedKey, map);
               } catch (Exception ex) {
                  // If the config is not there or there is a problem check if we need to default to null
                  if (annotation.defaultToNull()) {
                     return null;
                  } else {
                     throw new IllegalArgumentException(String.format("Unable to find or determine Map generic types for injected configuration field '%s'", target), ex);
                  }
               }
            }

            return map;
         }
      }

      throw new RuntimeException(String.format("Unable to inject Map configuration value into field '%s'", target));
   }

   @Produces
   @ConfigurationList(element = "")
   public <L extends List> CList<L> produceListDelegate(InjectionPoint injectionPoint) {
      ConfigurationList annotation = (ConfigurationList) injectionPoint.getQualifiers().stream().filter(a -> a.annotationType().equals(ConfigurationList.class)).findFirst().orElse(null);
      String target = (null == injectionPoint.getBean() ? "Dynamic lookup" : injectionPoint.getMember().getDeclaringClass().getSimpleName() + "." + injectionPoint.getMember().getName());

      if (null != annotation) {
         if (null != annotation.element() && !annotation.element().isEmpty()) {
            String expandedKey = expandKey(annotation.element());

            CList<L> list = (CList<L>) delegates.get(expandedKey);
            if (null == list) {
               try {            
                  if (null != injectionPoint.getMember()) {
                     // Injected
                     Field f = injectionPoint.getMember().getDeclaringClass().getDeclaredField(injectionPoint.getMember().getName());
                     list = new CList<>(expandedKey, annotation.subElementMapping(), f, config, configurationValues);
                  } else {
                     // Literal
                     // Example Usage: See ConfigurationStructureLiteral.java
                     ParameterizedType type = (ParameterizedType)injectionPoint.getType();
                     list = new CList<>(expandedKey, annotation.subElementMapping(), type, config, configurationValues);
                  }                  
                  delegates.put(expandedKey, list);
               } catch (Exception ex) {
                  // If the config is not there or there is a problem check if we need to default to null
                  if (annotation.defaultToNull()) {
                     return null;
                  } else {
                     throw new IllegalArgumentException(String.format("Unable to find or determine List generic type for injected configuration field '%s'", target), ex);
                  }
               }
            }

            return list;
         }
      }

      throw new RuntimeException(String.format("Unable to inject List configuration value into field '%s'", target));
   }

   @Produces
   @ConfigurationStructure(element = "")
   public <S> CStructure<S> produceStructureDelegate(InjectionPoint injectionPoint) {
      ConfigurationStructure annotation = (ConfigurationStructure) injectionPoint.getQualifiers().stream().filter(a -> a.annotationType().equals(ConfigurationStructure.class)).findFirst().orElse(null);
      String target = (null == injectionPoint.getBean() ? "Dynamic lookup" : injectionPoint.getMember().getDeclaringClass().getSimpleName() + "." + injectionPoint.getMember().getName());

      if (null != annotation) {
         if (null != annotation.element() && !annotation.element().isEmpty()) {
            String expandedKey = expandKey(annotation.element());

            CStructure<S> structure = (CStructure<S>) delegates.get(expandedKey);
            if (null == structure) {
               try {      
                  if (null != injectionPoint.getMember()) {
                     // Injected
                     Field f = injectionPoint.getMember().getDeclaringClass().getDeclaredField(injectionPoint.getMember().getName());
                     structure = new CStructure<>(expandedKey, f, config, configurationValues);                    
                  } else {
                     // Literal
                     // Example Usage: See ConfigurationStructureLiteral.java                     
                     ParameterizedType type = (ParameterizedType)injectionPoint.getType();
                     structure = new CStructure<>(expandedKey, type, config, configurationValues);                                                              
                  }                  
                  delegates.put(expandedKey, structure);
               } catch (Exception ex) {
                  // If the config is not there or there is a problem check if we need to default to null
                  if (annotation.defaultToNull()) {
                     return null;
                  } else {
                     throw new IllegalArgumentException(String.format("Unable to find or determine Structure generic type for injected configuration field '%s'", target), ex);
                  }
               }
            }

            return structure;
         }
      }

      throw new RuntimeException(String.format("Unable to inject Structure configuration value into field '%s'", target));
   }

   @Produces
   @Configuration(element = "")
   public CInteger produceIntegerDelegate(InjectionPoint injectionPoint) {
      CInteger delegate = produceDelegate(injectionPoint);
      return delegate;
   }

   @Produces
   @Configuration(element = "")
   public CLong produceLongDelegate(InjectionPoint injectionPoint) {
      CLong delegate = produceDelegate(injectionPoint);
      return delegate;
   }

   @Produces
   @Configuration(element = "")
   public CFloat produceFloatDelegate(InjectionPoint injectionPoint) {
      CFloat delegate = produceDelegate(injectionPoint);
      return delegate;
   }

   @Produces
   @Configuration(element = "")
   public CDouble produceDoubleDelegate(InjectionPoint injectionPoint) {
      CDouble delegate = produceDelegate(injectionPoint);
      return delegate;
   }

   @Produces
   @Configuration(element = "")
   public CString produceStringDelegate(InjectionPoint injectionPoint) {
      CString delegate = produceDelegate(injectionPoint);
      return delegate;
   }

   @Produces
   @Configuration(element = "")
   public CBoolean produceBooleanDelegate(InjectionPoint injectionPoint) {
      CBoolean delegate = produceDelegate(injectionPoint);
      return delegate;
   }

   @Produces
   @Configuration(element = "")
   public CTimeDuration produceTimeDurationDelegate(InjectionPoint injectionPoint) {
      CTimeDuration delegate = produceDelegate(injectionPoint);
      return delegate;
   }

   public <T> T produceDelegate(InjectionPoint injectionPoint) {
      // Use qualifiers as allows for both dynamic (with literal) and injected lookups 
      Configuration annotation = (Configuration) injectionPoint.getQualifiers().stream().filter(a -> a.annotationType().equals(Configuration.class)).findFirst().orElse(null);
      String target = (null == injectionPoint.getBean() ? "Dynamic lookup" : injectionPoint.getMember().getDeclaringClass().getSimpleName() + "." + injectionPoint.getMember().getName());

      if (null != annotation) {
         if (null != annotation.element() && !annotation.element().isEmpty()) {
            try {
               String expandedKey = expandKey(annotation.element());

               // Return existing delegate if it exists
               ConfigurationValue cv = delegates.get(expandedKey);
               if (null == cv) {
                  // Create new delegate for element
                  String defaultValue = DEFAULT.equals(annotation.defaultValue()) ? null : annotation.defaultValue();

                  // Type
                  switch (injectionPoint.getType().getTypeName()) {
                     case "org.rogueware.configuration.delegate.CInteger":
                        cv = new CInteger(expandedKey, defaultValue, config, configurationValues);
                        break;

                     case "org.rogueware.configuration.delegate.CLong":
                        cv = new CLong(expandedKey, defaultValue, config, configurationValues);
                        break;

                     case "org.rogueware.configuration.delegate.CFloat":
                        cv = new CFloat(expandedKey, defaultValue, config, configurationValues);
                        break;

                     case "org.rogueware.configuration.delegate.CDouble":
                        cv = new CDouble(expandedKey, defaultValue, config, configurationValues);
                        break;

                     case "org.rogueware.configuration.delegate.CString":
                        cv = new CString(expandedKey, defaultValue, config, configurationValues);
                        break;

                     case "org.rogueware.configuration.delegate.CBoolean":
                        cv = new CBoolean(expandedKey, defaultValue, config, configurationValues);
                        break;

                     case "org.rogueware.configuration.delegate.CTimeDuration":
                        cv = new CTimeDuration(expandedKey, defaultValue, config, configurationValues);
                        break;

                     default:
                        throw new IllegalArgumentException(String.format("Configuration value cannot be injected into field '%s' that is not of type ConfigurationValue", target));
                  }

                  delegates.put(expandedKey, cv);
               }

               return (T) cv;
            } catch (Throwable ex) {
               throw new IllegalArgumentException(String.format("Unable to inject configuration value into field '%s'", target), ex);
            }
         }
      }

      throw new RuntimeException(String.format("Unable to inject configuration value into field '%s'", target));
   }

   private void readConfigurationFile() {

      try {
         File cFile = new File(configFile);
         if (!cFile.exists()) {
            System.err.println(String.format("Configuration file '%s' does not exist", configFile));
            throw new RuntimeException(String.format("Configuration file '%s' does not exist", configFile));
         }
         if (!cFile.isFile() || !cFile.canRead()) {
            System.err.println(String.format("Configuration file '%s' is not a file or is not readable", configFile));
            throw new RuntimeException(String.format("Configuration file '%s' is not a file or is not readable", configFile));
         }

         // Load from stream that will substitute system properties and env variables before parsing the xml
         InputStream is = new EnvSystemValueInputStream(new FileInputStream(configFile));
         config = new XMLConfiguration();
         config.load(is);
         config.setThrowExceptionOnMissing(true);

         parsedLastModified = cFile.lastModified();
         loadedLastModified = cFile.lastModified();

         // If we could read the configuration, reset the values
         configurationValues.clear();

         System.out.println(String.format("Initialized configuration using file '%s' with last modified timestamp '%s", configFile, sdf.format(new Date(cFile.lastModified()))));
      } catch (org.apache.commons.configuration.ConfigurationException | IOException ex) {
         System.err.println(String.format("Unable to load configuration file '%s'", configFile));
         ex.printStackTrace(System.err);
         throw new RuntimeException(String.format("Unable to load configuration file '%s'", configFile), ex);
      }

   }

   private String expandKey(String key) {
      // Add the root if not already present
      if (key.startsWith(".")) {
         key = (configurationRoot + key).replace("..", ".");
      }
      if (key.startsWith(".")) {
         key = key.substring(1);   // Get rid of any leading . (Configuration root may be empty)
      }
      return key;
   }

   @Override
   public void run() {
      // Poll for configuration change and reload indicator
      try {
         File cFile = new File(configFile);
         File cReloadFile = new File(configFile + ".reload");

         // If new file, first parse before trying to load
         if (cFile.lastModified() > parsedLastModified) {
            System.out.println(String.format("Detected updated configuration file '%s' with modified date '%s'", configFile, sdf.format(new Date(cFile.lastModified()))));
            try {
               XMLConfiguration parsed = new XMLConfiguration(cFile);
               parsedLastModified = cFile.lastModified();

               System.out.println(String.format("Touch the reload file '%s.reload' for the configuration to be loaded", configFile));
            } catch (Exception ex) {
               System.err.println(String.format("Detected updated configuration file '%s' with modified date '%s' did not pass parsing, aborting configuration loading", configFile, sdf.format(new Date(cFile.lastModified()))));
               ex.printStackTrace(System.err);
            }
         }

         // Only reload if parsed
         if (cFile.lastModified() > loadedLastModified && cFile.lastModified() == parsedLastModified) {
            // Check if there is a reload file

            if (cReloadFile.exists() && cReloadFile.isFile() && cReloadFile.canWrite()) {
               if (null != config) {
                  config.refresh();    // Reloads the configuration
                  // If we could read the configuration, reset the values for delegated to pick up new values
                  configurationValues.clear();
                  cReloadFile.delete();
                  System.out.println(String.format("Loaded updated configuration file '%s' with modified date '%s'", configFile, sdf.format(new Date(cFile.lastModified()))));

                  loadedLastModified = cFile.lastModified();

                  // Notify of configuration change
                  ConfigurationChanged cc = new ConfigurationChanged(configFile, new Date(cFile.lastModified()));
                  configurationChangedEvent.fire(cc);
               }
            }
         }
      } catch (Throwable ex) {
      }
   }

   public Map<String, Object> getCachedConfigurationValues() {
      return configurationValues;
   }
   

}
