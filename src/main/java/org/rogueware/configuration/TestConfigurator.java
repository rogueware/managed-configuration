/*
 * Copyright 2025 Johnathan Ingram.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.rogueware.configuration;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@ApplicationScoped
public class TestConfigurator {
   // Note: This class can be injected into test cases to change the running config during testing
   //       Note recommended to use in production code
      
   @Inject 
   Configurator configurator;   
      
  
   public Map<String, Object> getConfigurationValues() {
      return new HashMap<>(configurator.getCachedConfigurationValues());
   }
   
   
   public Object getConfigurationValue(String codeInjectionKey) {
      return configurator.getCachedConfigurationValues().get(codeInjectionKey);
   }
   
   public void setConfigurationValue(String codeInjectionKey, Object value) {
      configurator.getCachedConfigurationValues().put(codeInjectionKey, value);
   }   
   
}
