/*
 * Copyright 2017 jingram.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.rogueware.configuration.cdi.extension;

import jakarta.enterprise.event.Observes;
import jakarta.enterprise.inject.spi.AnnotatedConstructor;
import jakarta.enterprise.inject.spi.AnnotatedField;
import jakarta.enterprise.inject.spi.AnnotatedMethod;
import jakarta.enterprise.inject.spi.AnnotatedType;
import jakarta.enterprise.inject.spi.Extension;
import jakarta.enterprise.inject.spi.ProcessAnnotatedType;
import jakarta.enterprise.inject.spi.WithAnnotations;
import jakarta.enterprise.util.AnnotationLiteral;
import jakarta.inject.Inject;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.rogueware.configuration.qualifier.Configuration;
import org.rogueware.configuration.qualifier.ConfigurationList;
import org.rogueware.configuration.qualifier.ConfigurationMap;
import org.rogueware.configuration.qualifier.ConfigurationStructure;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class ConfigurationExtension implements Extension {

   // Ensure all fields annotated with @Conifugration are annotated with @Inject
   public <T> void processEJBAnnotations(@Observes @WithAnnotations({Configuration.class, ConfigurationMap.class, ConfigurationList.class, ConfigurationStructure.class}) ProcessAnnotatedType<T> pat) {
      final AnnotatedType<T> at = pat.getAnnotatedType();
      final Class klass = pat.getAnnotatedType().getJavaClass();

      // Add @Inject to any field declared with @Configuration, @ConfigurationMap, @ConfigurationList, @ConfigurationManager
      AnnotatedType<T> wrapped = new AnnotatedType<T>() {

         @Override
         public Class<T> getJavaClass() {
            return at.getJavaClass();
         }

         @Override
         public Set<AnnotatedConstructor<T>> getConstructors() {
            return at.getConstructors();
         }

         @Override
         public Set<AnnotatedMethod<? super T>> getMethods() {
            return at.getMethods();
         }

         @Override
         public Set<AnnotatedField<? super T>> getFields() {
            Set<AnnotatedField<? super T>> result = new HashSet<>();
            for (final AnnotatedField af : at.getFields()) {
               // If there is a field with the @Configuration, @ConfigurationMap @ConfigurationList, @ConfigurationManager then
               // add @Inject as its within CDI
               if (af.isAnnotationPresent(Configuration.class)
                       || af.isAnnotationPresent(ConfigurationMap.class)
                       || af.isAnnotationPresent(ConfigurationStructure.class)
                       || af.isAnnotationPresent(ConfigurationList.class)) {
                  result.add(addAnnotation(af, new AnnotationLiteral<Inject>() {
                  }));
               } else {
                  result.add(af);
               }
            }

            return result;
         }

         @Override
         public Type getBaseType() {
            return at.getBaseType();
         }

         @Override
         public Set<Type> getTypeClosure() {
            return at.getTypeClosure();
         }

         @Override
         public <T extends Annotation> T getAnnotation(Class<T> annotationType) {
            return at.getAnnotation(annotationType);
         }

         @Override
         public Set<Annotation> getAnnotations() {
            return at.getAnnotations();
         }

         @Override
         public boolean isAnnotationPresent(Class<? extends Annotation> annotationType) {
            return at.isAnnotationPresent(annotationType);
         }
      };

      pat.setAnnotatedType(wrapped);
   }

   public static AnnotatedField addAnnotation(final AnnotatedField af, final Annotation... annotations) {

      return new AnnotatedField() {
         @Override
         public Field getJavaMember() {
            return af.getJavaMember();
         }

         @Override
         public boolean isStatic() {
            return af.isStatic();
         }

         @Override
         public AnnotatedType getDeclaringType() {
            return af.getDeclaringType();
         }

         @Override
         public Type getBaseType() {
            return af.getBaseType();
         }

         @Override
         public Set<Type> getTypeClosure() {
            return af.getTypeClosure();
         }

         @Override
         public <T extends Annotation> T getAnnotation(Class<T> annotationType) {
            for (Annotation a : annotations) {
               if (a.annotationType().equals(annotationType)) {
                  return (T) a;
               }
            }
            return af.getAnnotation(annotationType);
         }

         @Override
         public Set<Annotation> getAnnotations() {
            Set<Annotation> result = new HashSet<>(af.getAnnotations());
            result.addAll(Arrays.asList(annotations));
            return result;
         }

         @Override
         public boolean isAnnotationPresent(Class<? extends Annotation> annotationType) {
            for (Annotation a : annotations) {

               if (a.annotationType().equals(annotationType)) {
                  return true;
               }
            }

            return af.isAnnotationPresent(annotationType);
         }
      };
   }

   public static AnnotatedMethod addAnnotation(final AnnotatedMethod am, final Annotation... annotations) {
      return new AnnotatedMethod() {

         @Override
         public Method getJavaMember() {
            return am.getJavaMember();
         }

         @Override
         public List getParameters() {
            return am.getParameters();
         }

         @Override
         public boolean isStatic() {
            return am.isStatic();
         }

         @Override
         public AnnotatedType getDeclaringType() {
            return am.getDeclaringType();
         }

         @Override
         public Type getBaseType() {
            return am.getBaseType();
         }

         @Override
         public Set<Type> getTypeClosure() {
            return am.getTypeClosure();
         }

         @Override
         public <T extends Annotation> T getAnnotation(Class<T> annotationType) {
            for (Annotation a : annotations) {
               if (a.annotationType().equals(annotationType)) {
                  return (T) a;
               }
            }
            return am.getAnnotation(annotationType);
         }

         @Override
         public Set<Annotation> getAnnotations() {
            Set<Annotation> result = new HashSet<>(am.getAnnotations());
            result.addAll(Arrays.asList(annotations));
            return result;
         }

         @Override
         public boolean isAnnotationPresent(Class<? extends Annotation> annotationType) {
            for (Annotation a : annotations) {
               if (a.annotationType().equals(annotationType)) {
                  return true;
               }
            }

            return am.isAnnotationPresent(annotationType);
         }
      };
   }
}
