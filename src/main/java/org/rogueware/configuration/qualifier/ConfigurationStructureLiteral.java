/*
 * Copyright 2017 jingram.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.rogueware.configuration.qualifier;

import jakarta.enterprise.util.AnnotationLiteral;


/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@SuppressWarnings("AnnotationAsSuperInterface")
public class ConfigurationStructureLiteral extends AnnotationLiteral<ConfigurationStructure> implements ConfigurationStructure {

   // Example Usage:
   //    TypeLiteral imageProcessingScaleConfigType = new TypeLiteral<CStructure<ImageProcessingScaleConfig>>(){};      
   //    ConfigurationStructureLiteral scaleLiteral = new ConfigurationStructureLiteral(".ImageProcessing.Certificate", false);      
   //    scaleConfig = ((CStructure<ImageProcessingScaleConfig>)CDI.current().select(imageProcessingScaleConfigType, scaleLiteral).get()).structure();      
   
   private final String element;
   private final boolean defaultToNull;

   public ConfigurationStructureLiteral(String element) {
      this.element = element;
      this.defaultToNull = false;
   }

   public ConfigurationStructureLiteral(String element, boolean defaultToNull) {
      this.element = element;
      this.defaultToNull = defaultToNull;
   }

   @Override
   public String element() {
      return element;
   }

   @Override
   public boolean defaultToNull() {
      return defaultToNull;
   }

}
