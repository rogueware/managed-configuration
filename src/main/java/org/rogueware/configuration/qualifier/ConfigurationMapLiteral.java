/*
 * Copyright 2017 jingram.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.rogueware.configuration.qualifier;

import jakarta.enterprise.util.AnnotationLiteral;
import org.rogueware.configuration.Configurator;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@SuppressWarnings("AnnotationAsSuperInterface")
public class ConfigurationMapLiteral extends AnnotationLiteral<ConfigurationMap> implements ConfigurationMap {

   // Example Usage: See ConfigurationStructureLiteral.java
           
   private final String element;
   private final String subElementMapping;
   private final String mapKeyAttribute;
   private final String mapValueAttribute;
   private final boolean defaultToNull;

   public ConfigurationMapLiteral(String element, String subElementMapping, String mapKeyAttribute) {
      this.element = element;
      this.subElementMapping = subElementMapping;
      this.mapKeyAttribute = mapKeyAttribute;
      this.mapValueAttribute = Configurator.DEFAULT;
      this.defaultToNull = false;
   }

   public ConfigurationMapLiteral(String element, String subElementMapping, String mapKeyAttribute, String mapValueAttribute, boolean defaultToNull) {
      this.element = element;
      this.subElementMapping = subElementMapping;
      this.mapKeyAttribute = mapKeyAttribute;
      this.mapValueAttribute = mapValueAttribute;
      this.defaultToNull = defaultToNull;
   }

   @Override
   public String element() {
      return element;
   }

   @Override
   public String subElementMapping() {
      return subElementMapping;
   }

   @Override
   public String mapKeyAttribute() {
      return mapKeyAttribute;
   }

   @Override
   public String mapValueAttribute() {
      return mapValueAttribute;
   }
   
   @Override
   public boolean defaultToNull() {
      return defaultToNull;
   }
}
