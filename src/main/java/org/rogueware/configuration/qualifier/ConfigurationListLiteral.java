/*
 * Copyright 2017 jingram.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.rogueware.configuration.qualifier;

import jakarta.enterprise.util.AnnotationLiteral;
import org.rogueware.configuration.Configurator;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@SuppressWarnings("AnnotationAsSuperInterface")
public class ConfigurationListLiteral extends AnnotationLiteral<ConfigurationList> implements ConfigurationList {

   // Example Usage: See ConfigurationStructureLiteral.java
   
   private final String element;
   private final String subElementMapping;
   private final boolean defaultToNull;

   public ConfigurationListLiteral(String element) {
      this.element = element;
      this.subElementMapping = Configurator.DEFAULT;
      this.defaultToNull = false;
   }

   public ConfigurationListLiteral(String element, boolean defaultToNull) {
      this.element = element;
      this.subElementMapping = Configurator.DEFAULT;
      this.defaultToNull = defaultToNull;
   }

   public ConfigurationListLiteral(String element, String subElementMapping, boolean defaultToNull) {
      this.element = element;
      this.subElementMapping = subElementMapping;
      this.defaultToNull = defaultToNull;
   }

   @Override
   public String element() {
      return element;
   }

   @Override
   public String subElementMapping() {
      return subElementMapping;
   }

   @Override
   public boolean defaultToNull() {
      return defaultToNull;
   }

}
