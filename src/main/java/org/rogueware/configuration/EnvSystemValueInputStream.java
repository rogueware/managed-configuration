/*
 * Copyright 2017 jingram.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.rogueware.configuration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class EnvSystemValueInputStream extends InputStream {

    InputStream is;
    BufferedReader br;

    int index = 0;
    char[] chars;

    public EnvSystemValueInputStream(InputStream is) {
        this.is = is;

        // Read an entire line from the parent
        InputStreamReader isr = new InputStreamReader(is);
        br = new BufferedReader(isr);
    }

    @Override
    public int read() throws IOException {
        if (null == chars || index >= chars.length) 
            if (!readLine()) return -1;    // Readline or return end of stream if no more data
            
        return chars[index++];
    }

    @Override
    public void close() throws IOException {
        try {
            br.close();
        } catch(Exception ex) {}
    
        try {
            is.close();
        } catch(Exception ex) {}
    }

    private boolean readLine() throws IOException {
        String line = br.readLine();
        if (null == line) return false;    // End of file

        // Replace any env or system properties references in the line '${key}''
        line = substituteEnvPropVariables(line) + "\n";
        
        // Make the characters available to the stream
        index = 0;
        chars = line.toCharArray();
        return true;
    }

    private String substituteEnvPropVariables(String val) {
        // Substitute any system parameters
        for (Object ko : System.getProperties().keySet()) {
            if (ko instanceof String) {
                String k = (String)ko;
                if (val.contains("${" + k + "}")) {
                    try {
                        String sysValue = System.getProperty(k);
                        val = val.replaceAll("\\$\\{" + k + "\\}", sysValue);
                    } catch (Exception ex) {
                        // :) Can't do anything, param not substituted
                    }
                }
            }
        }

        // Substitute any environment variables
        for (String k : System.getenv().keySet()) {
            if (val.contains("${" + k + "}")) {
                try {
                    String envValue = System.getenv(k);
                    val = val.replaceAll("\\$\\{" + k + "\\}", envValue);
                } catch (Exception ex) {
                    // :) Can't do anything, param not substituted
                }
            }
        }

        return val;
    }    
}    

