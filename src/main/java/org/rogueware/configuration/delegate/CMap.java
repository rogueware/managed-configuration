/*
 * Copyright 2017 jingram.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.rogueware.configuration.delegate;

import jakarta.enterprise.inject.Vetoed;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.rogueware.configuration.Configurator;
import static org.rogueware.configuration.Configurator.DEFAULT;
import org.rogueware.configuration.annotation.ConfigurationAttribute;
import org.rogueware.configuration.qualifier.Configuration;
import org.rogueware.configuration.qualifier.ConfigurationList;
import org.rogueware.configuration.qualifier.ConfigurationMap;
import org.rogueware.configuration.qualifier.ConfigurationStructure;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Vetoed
public class CMap<M extends Map> implements ConfigurationValue {

   String key;
   String mapKeyAttr;
   String mapValueAttr;
   Class mapKeyClass;
   Class mapValClass;
   HierarchicalConfiguration config;
   Map<String, Object> configurationValues;   // Values will change when new config loaded

   public CMap(String key, String mapKeyAttr, String mapValueAttr, Field injectedField, HierarchicalConfiguration config, Map<String, Object> configurationValues) {
      // Injected
      this.key = key;
      this.mapKeyAttr = mapKeyAttr;
      this.mapValueAttr = mapValueAttr;  // If not using composite object for value

      // Determine the Map key and value class for the generic type
      try {
         // Must be a generic type of Map
         ParameterizedType pt = (ParameterizedType) injectedField.getGenericType();
         Type mapType = pt.getActualTypeArguments()[0];
         String mapTypeName = mapType.getTypeName();

         String keyClassName = mapTypeName.split(",")[0].replaceFirst("java.util.Map<", "").trim();

         String tmp = mapTypeName.split(",")[1];
         int pos = tmp.indexOf('<');  // May be a templated type
         String valClassName = pos > 0 ? tmp.substring(0, pos).trim() : tmp;
         valClassName = valClassName.replace(">", "").trim();

         this.mapKeyClass = Class.forName(keyClassName);
         this.mapValClass = Class.forName(valClassName);
      } catch (Exception ex) {
         throw new IllegalArgumentException(String.format("Unable to find or determine Map generic types for injected configuration field '%s'", injectedField.getName()), ex);
      }

      this.config = config;
      this.configurationValues = configurationValues;

      // Store the value on creation (First injection)
      storeConfigurationValue();
   }

   public CMap(String key, String mapKeyAttr, String mapValueAttr, ParameterizedType pt, HierarchicalConfiguration config, Map<String, Object> configurationValues) {
      // Literal
      this.key = key;
      this.mapKeyAttr = mapKeyAttr;
      this.mapValueAttr = mapValueAttr;  // If not using composite object for value

      // Determine the Map key and value class for the generic type
      try {
         // Must be a generic type of Map
         Type mapType = pt.getActualTypeArguments()[0];
         String mapTypeName = mapType.getTypeName();

         String keyClassName = mapTypeName.split(",")[0].replaceFirst("java.util.Map<", "").trim();

         String tmp = mapTypeName.split(",")[1];
         int pos = tmp.indexOf('<');  // May be a templated type
         String valClassName = pos > 0 ? tmp.substring(0, pos).trim() : tmp;
         valClassName = valClassName.replace(">", "").trim();

         this.mapKeyClass = Class.forName(keyClassName);
         this.mapValClass = Class.forName(valClassName);
      } catch (Exception ex) {
         throw new IllegalArgumentException("Unable to find or determine Map generic types for LITERAL lookup", ex);
      }

      this.config = config;
      this.configurationValues = configurationValues;

      // Store the value on creation (First injection)
      storeConfigurationValue();
   }
   
   
   private void storeConfigurationValue() {
      Map map = Collections.synchronizedMap(new LinkedHashMap());   // Preserve insertion order

      List<HierarchicalConfiguration> subElements = config.configurationsAt(key.replace("..", "."));
      int index = 0;
      for (HierarchicalConfiguration subElement : subElements) {
         try {
            // Create the value object
            Object val;
            if (DelegateUtility.BASE_CLASSES.contains(mapValClass) && !Configurator.DEFAULT.equals(mapValueAttr)) {
               // Supported Primitive Wrappers, String & String[]
               val = DelegateUtility.getConfigurationAttributeValue(subElement, mapValueAttr, mapValClass, Configurator.DEFAULT);
            } else {
               // Annotated class
               val = mapValClass.newInstance();

               for (Field f : DelegateUtility.getAllDeclaredClassFields(mapValClass)) {
                  // ConfigurationAttribute mappings
                  {
                     ConfigurationAttribute annotation = f.getAnnotation(ConfigurationAttribute.class);
                     if (null != annotation) {
                        f.setAccessible(true);
                        f.set(val, DelegateUtility.getConfigurationAttributeValue(subElement, annotation.attribute(), f.getType(), annotation.defaultValue()));
                        continue;
                     }
                  }

                  // ConfigurationMap mappings
                  {
                     ConfigurationMap annotation = f.getAnnotation(ConfigurationMap.class);
                     if (null != annotation) {
                        f.setAccessible(true);
                        String subKey = (key + "(" + index + ")" + "." + annotation.element() + "." + annotation.subElementMapping()).replace("..", ".");

                        // Set value handling defaulting to null if there is an error
                        try {
                           f.set(val, new CMap<>(subKey, annotation.mapKeyAttribute(), annotation.mapValueAttribute(), f, config, configurationValues));
                        } catch (Exception ex) {
                           if (annotation.defaultToNull()) {
                              f.set(val, null);
                           } else {
                              throw ex;
                           }
                        }
                        continue;
                     }
                  }

                  // ConfigurationList mappings
                  {
                     ConfigurationList annotation = f.getAnnotation(ConfigurationList.class);
                     if (null != annotation) {
                        f.setAccessible(true);
                        // Ignore element that references "."
                        String subKey = (key + "(" + index + ")" + (annotation.element().equals(".") ? "" : "." + annotation.element())).replace("..", ".");
                        
                        // Set value handling defaulting to null if there is an error
                        try {
                           f.set(val, new CList<>(subKey, annotation.subElementMapping(), f, config, configurationValues));
                        } catch (Exception ex) {
                           if (annotation.defaultToNull()) {
                              f.set(val, null);
                           } else {
                              throw ex;
                           }
                        }
                        continue;
                     }
                  }

                  // ConfigurationStructure mappings
                  {
                     ConfigurationStructure annotation = f.getAnnotation(ConfigurationStructure.class);
                     if (null != annotation) {
                        f.setAccessible(true);
                        String subKey = (key + "(" + index + ")" + "." + annotation.element()).replace("..", ".");

                        // Set value handling defaulting to null if there is an error
                        try {
                           f.set(val, new CStructure<>(subKey, f, config, configurationValues));
                        } catch (Exception ex) {
                           if (annotation.defaultToNull()) {
                              f.set(val, null);
                           } else {
                              throw ex;
                           }
                        }
                        continue;
                     }
                  }

                  // Configuration mappings
                  {
                     Configuration annotation = f.getAnnotation(Configuration.class);
                     if (null != annotation) {
                        f.setAccessible(true);
                        String subKey = (key + "(" + index + ")" + "." + annotation.element()).replace("..", ".");
                        String defaultValue = DEFAULT.equals(annotation.defaultValue()) ? null : annotation.defaultValue();
                        switch (f.getType().getName()) {
                           case "org.rogueware.configuration.delegate.CInteger":
                              f.set(val, new CInteger(subKey, defaultValue, config, configurationValues));
                              break;
                           case "org.rogueware.configuration.delegate.CLong":
                              f.set(val, new CLong(subKey, defaultValue, config, configurationValues));
                              break;
                           case "org.rogueware.configuration.delegate.CFloat":
                              f.set(val, new CFloat(subKey, defaultValue, config, configurationValues));
                              break;
                           case "org.rogueware.configuration.delegate.CDouble":
                              f.set(val, new CDouble(subKey, defaultValue, config, configurationValues));
                              break;
                           case "org.rogueware.configuration.delegate.CString":
                              f.set(val, new CString(subKey, defaultValue, config, configurationValues));
                              break;
                           case "org.rogueware.configuration.delegate.CBoolean":
                              f.set(val, new CBoolean(subKey, defaultValue, config, configurationValues));
                              break;
                           case "org.rogueware.configuration.delegate.CTimeDuration":
                              f.set(val, new CTimeDuration(subKey, defaultValue, config, configurationValues));
                              break;
                           default:
                              throw new IllegalArgumentException(String.format("Configuration value cannot be injected into field '%s' that is not of type ConfigurationValue", f.getName()));
                        }
                     }
                  }
               }
            }

            // Create the key object
            Object key = DelegateUtility.getConfigurationAttributeValue(subElement, mapKeyAttr, mapKeyClass, Configurator.DEFAULT);

            // Add the element to the map
            map.put(key, val);
         } catch (InstantiationException | IllegalAccessException ex) {
            throw new IllegalArgumentException(String.format("Unable to create map value for key '%s' with value class '%s'", key, mapValClass.getName()), ex);
         }

         index++;
      }

      configurationValues.put(key, map);
   }

   private <T> T getStoredConfigurationValue() {
      if (!configurationValues.containsKey(getKey())) {
         storeConfigurationValue();
      }
      return (T) configurationValues.get(getKey());
   }

   @Override
   public String getKey() {
      return key;
   }

   @Override
   public String toString() {
      M map = map();

      StringBuilder sb = new StringBuilder();
      sb.append(key);
      sb.append("=\n");
      for (Object k : map.keySet()) {
         sb.append("   ");
         sb.append(k);
         sb.append("->");
         sb.append(map.get(k));
         sb.append("\n");
      }
      return sb.toString();
   }

   @Override
   public String strValue() {
      return "";
   }

   public M map() {
      // Return reference to map .. then if map replaced in store, existing execution runs with it until out of scope
      // This should allow iteration over map to complete on old map and when requesting next copy, can then run on any new map from config reload
      M map = getStoredConfigurationValue();
      return map;
   }

}
