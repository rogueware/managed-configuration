/*
 * Copyright 2017 jingram.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.rogueware.configuration.delegate;

import jakarta.enterprise.inject.Vetoed;
import java.util.Map;
import org.apache.commons.configuration.HierarchicalConfiguration;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Vetoed
public class CInteger extends ConfigurationNumberValue implements Comparable<Integer> {

   public CInteger(String key, String defaultValue, HierarchicalConfiguration config, Map<String, Object> configurationValues) {
      super(key, defaultValue, config, configurationValues);

      // Store the value on creation (First injection)
      storeConfigurationValue();
   }

   @Override
   protected final void storeConfigurationValue() {
      // Store a specific integer value
      int val;
      if (null != defaultValue) {
         int defVal = Integer.parseInt(defaultValue);
         val = config.getInt(key, defVal);
      } else {
         val = config.getInt(key);
      }

      configurationValues.put(key, val);
   }

   @Override
   public final String strValue() {
      return Integer.toString(intValue());
   }

   public final String strHexValue() {
      return Integer.toHexString(intValue());
   }

   @Override
   public int intValue() {
      return getStoredConfigurationValue();
   }

   @Override
   public long longValue() {
      return (long)intValue();
   }

   @Override
   public float floatValue() {
      return (float) intValue();
   }

   @Override
   public double doubleValue() {
      return (double) intValue();
   }
   
   @Override
   public int compareTo(Integer o) {
      Integer val = intValue();
      return val.compareTo(o);
   }

   @Override
   public boolean equals(Object obj) {
      Integer val = intValue();
      return val.equals(obj);
   }   
}
