/*
 * Copyright 2017 jingram.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.rogueware.configuration.delegate;

import jakarta.enterprise.inject.Vetoed;
import java.util.Map;
import org.apache.commons.configuration.HierarchicalConfiguration;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Vetoed
public class CBoolean implements ConfigurationValue, Comparable<Boolean> {

   String key;
   String defaultValue;
   HierarchicalConfiguration config;
   Map<String, Object> configurationValues;   // Values will change when new config loaded

   public CBoolean(String key, String defaultValue, HierarchicalConfiguration config, Map<String, Object> configurationValues) {
      this.key = key;
      this.defaultValue = defaultValue;
      this.config = config;
      this.configurationValues = configurationValues;

      // Store the value on creation (First injection)
      storeConfigurationValue();
   }

   protected final void storeConfigurationValue() {
      // Store a specific boolean value
      boolean val;
      if (null != defaultValue) {
         boolean defVal = Boolean.parseBoolean(defaultValue);
         val = config.getBoolean(key, defVal);
      } else {
         val = config.getBoolean(key);
      }

      configurationValues.put(key, val);
   }

   protected <T> T getStoredConfigurationValue() {
      if (!configurationValues.containsKey(getKey())) {
         storeConfigurationValue();
      }
      return (T) configurationValues.get(getKey());
   }

   @Override
   public String getKey() {
      return key;
   }

   @Override
   public final String strValue() {
      return Boolean.toString(boolValue());
   }

   public boolean boolValue() {
      return getStoredConfigurationValue();
   }

   @Override
   public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append(getKey());
      sb.append("=");
      sb.append(strValue());
      return sb.toString();
   }

   @Override
   public boolean equals(Object anObject) {
      Boolean val = boolValue();
      return val.equals(anObject);
   }

   @Override
   public int compareTo(Boolean o) {
      Boolean val = boolValue();
      return val.compareTo(o);
   }

}
