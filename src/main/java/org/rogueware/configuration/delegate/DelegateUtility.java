/*
 * Copyright 2017 jingram.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.rogueware.configuration.delegate;

import jakarta.enterprise.inject.Vetoed;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.rogueware.configuration.Configurator;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Vetoed
public class DelegateUtility {

   public static final Set<Class<?>> BASE_CLASSES = new HashSet<>();

   static {
      BASE_CLASSES.add(Byte.class);
      BASE_CLASSES.add(Short.class);
      BASE_CLASSES.add(Integer.class);
      BASE_CLASSES.add(Long.class);
      BASE_CLASSES.add(Float.class);
      BASE_CLASSES.add(Double.class);
      BASE_CLASSES.add(Boolean.class);
      BASE_CLASSES.add(String.class);
   }

   public static List<Field> getAllDeclaredClassFields(Class klass) {
      // Returns all fields declared including inherited fields for a class
      List<Field> fields = new ArrayList<>();

      // Need to get all fields, including fields form any parent classes
      Class c = klass;
      while (null != c) {
         fields.addAll(Arrays.asList(c.getDeclaredFields()));
         c = c.getSuperclass();

         if ("java.lang.Object".equals(c.getName())) {
            c = null;
         }
      }

      return fields;
   }

   public static Object getConfigurationAttributeValue(HierarchicalConfiguration subElement, String attribute, Class type, String defaultValue) {
      // If no attribute name, then get the value of the element / tag
      // Either full attribute name is passed in in form xxx[@attr], [@attr] or just the name that needs to be built into an attribute
      String attr = null == attribute 
              ? "" 
              : !attribute.contains("@") ? "[@" + attribute + "]" : attribute;
      
      defaultValue = defaultValue.equals(Configurator.DEFAULT) ? null : defaultValue;

      if (String.class == type) {

         String val = "";
         String[] vals = subElement.getStringArray(attr);
         if (null != defaultValue) {
            // Use get string array incase the value has a , that needs to be used.
            // Will need to put the string back together again
            if (0 == vals.length) {
               val = defaultValue;
            } else {
               val = String.join(",", vals);
            }
         } else {
            if (0 != vals.length) {
               val = String.join(",", vals);
            }
         }

         return val;
      }
      if (String[].class == type) {
         if (!subElement.containsKey(attr) || null == subElement.getString(attr) || subElement.getString(attr).isEmpty()) {
            return new String[0];
         } else {
            return subElement.getStringArray(attr);
         }
      }
      if (boolean.class == type || Boolean.class == type) {
         return null == defaultValue ? subElement.getBoolean(attr) : subElement.getBoolean(attr, Boolean.parseBoolean(defaultValue));
      }
      if (byte.class == type || Byte.class == type) {
         return null == defaultValue ? subElement.getByte(attr) : subElement.getByte(attr, Byte.parseByte(defaultValue));
      }
      if (short.class == type || Short.class == type) {
         return null == defaultValue ? subElement.getShort(attr) : subElement.getShort(attr, Short.parseShort(defaultValue));
      }
      if (int.class == type || Integer.class == type) {
         return null == defaultValue ? subElement.getInt(attr) : subElement.getInt(attr, Integer.parseInt(defaultValue));
      }
      if (long.class == type || Long.class == type) {
         return null == defaultValue ? subElement.getLong(attr) : subElement.getLong(attr, Long.parseLong(defaultValue));
      }
      if (float.class == type || Float.class == type) {
         return null == defaultValue ? subElement.getFloat(attr) : subElement.getFloat(attr, Float.parseFloat(defaultValue));
      }
      if (double.class == type || Double.class == type) {
         return null == defaultValue ? subElement.getDouble(attr) : subElement.getDouble(attr, Double.parseDouble(defaultValue));
      }
      throw new IllegalArgumentException(String.format("Cannot map structure value object field type '%s''", type.getName()));
   }

}
