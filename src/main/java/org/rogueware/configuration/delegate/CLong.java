/*
 * Copyright 2017 jingram.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.rogueware.configuration.delegate;

import jakarta.enterprise.inject.Vetoed;
import java.util.Map;
import org.apache.commons.configuration.HierarchicalConfiguration;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Vetoed
public class CLong extends ConfigurationNumberValue implements Comparable<Long> {

   public CLong(String key, String defaultValue, HierarchicalConfiguration config, Map<String, Object> configurationValues) {
      super(key, defaultValue, config, configurationValues);

      // Store the value on creation (First injection)
      storeConfigurationValue();
   }

   @Override
   protected final void storeConfigurationValue() {
      // Store a specific long value
      long val;
      if (null != defaultValue) {
         long defVal = Long.parseLong(defaultValue);
         val = config.getLong(key, defVal);
      } else {
         val = config.getLong(key);
      }

      configurationValues.put(key, val);
   }

   @Override
   public final String strValue() {
      return Long.toString(longValue());
   }

   public final String strHexValue() {
      return Long.toHexString(longValue());
   }

   @Override
   public int intValue() {
      return (int) longValue();
   }

   @Override
   public long longValue() {
      return getStoredConfigurationValue();
   }

   @Override
   public float floatValue() {
      return (float) longValue();
   }

   @Override
   public double doubleValue() {
      return (double) longValue();
   }

   @Override
   public int compareTo(Long o) {
      Long val = longValue();
      return val.compareTo(o);
   }

   @Override
   public boolean equals(Object obj) {
      Long val = longValue();
      return val.equals(obj);
   }
}
