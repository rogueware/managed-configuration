/*
 * Copyright 2017 jingram.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.rogueware.configuration.delegate;

import jakarta.enterprise.inject.Vetoed;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Map;
import org.apache.commons.configuration.HierarchicalConfiguration;
import static org.rogueware.configuration.Configurator.DEFAULT;
import org.rogueware.configuration.annotation.ConfigurationAttribute;
import org.rogueware.configuration.qualifier.Configuration;
import org.rogueware.configuration.qualifier.ConfigurationList;
import org.rogueware.configuration.qualifier.ConfigurationMap;
import org.rogueware.configuration.qualifier.ConfigurationStructure;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Vetoed
public class CStructure<S> implements ConfigurationValue {

   String key;
   HierarchicalConfiguration config;
   Map<String, Object> configurationValues;   // Values will change when new config loaded
   Class structureClass;

   public CStructure(String key, Field injectedField, HierarchicalConfiguration config, Map<String, Object> configurationValues) {
      // Injected
      this.key = key;
      this.config = config;
      this.configurationValues = configurationValues;

      // Obtain class for structure wrapper in order to create and populate correctly
      try {

         ParameterizedType pt = (ParameterizedType) injectedField.getGenericType();
         Type type = pt.getActualTypeArguments()[0];
         String typeName = type.getTypeName();
         this.structureClass = Class.forName(typeName);
      } catch (Exception ex) {
         throw new IllegalArgumentException(String.format("Unable to find or determine generic type for injected configuration field '%s'", injectedField.getName()), ex);
      }

      // Store the value on creation (First injection)
      storeConfigurationValue();
   }

   public CStructure(String key, ParameterizedType pt, HierarchicalConfiguration config, Map<String, Object> configurationValues) {
      // Literal
      this.key = key;
      this.config = config;
      this.configurationValues = configurationValues;

      // Obtain class for specific type
      try {
         Type type = pt.getActualTypeArguments()[0];
         String typeName = type.getTypeName();
         this.structureClass = Class.forName(typeName);
      } catch (Exception ex) {
         throw new IllegalArgumentException("Unable to find or determine generic type for LITERAL lookup", ex);
      }

      // Store the value on creation (First injection)
      storeConfigurationValue();
   }
   
   
   private void storeConfigurationValue() {
      HierarchicalConfiguration subElement = config.configurationAt(key.replace("..", "."));
      try {
         // Annotated class         
         Object val = structureClass.newInstance();
         for (Field f : DelegateUtility.getAllDeclaredClassFields(structureClass)) {
            // Attribute mapping
            {
               ConfigurationAttribute annotation = f.getAnnotation(ConfigurationAttribute.class);
               if (null != annotation) {
                  f.setAccessible(true);
                  f.set(val, DelegateUtility.getConfigurationAttributeValue(subElement, annotation.attribute(), f.getType(), annotation.defaultValue()));
               }
            }

            // ConfigurationMap mappings
            {
               ConfigurationMap annotation = f.getAnnotation(ConfigurationMap.class);
               if (null != annotation) {
                  f.setAccessible(true);
                  String subKey = (key + "." + annotation.element() + "." + annotation.subElementMapping()).replace("..", ".");

                  // Set value handling defaulting to null if there is an error
                  try {
                     f.set(val, new CMap<>(subKey, annotation.mapKeyAttribute(), annotation.mapValueAttribute(), f, config, configurationValues));
                  } catch (Exception ex) {
                     if (annotation.defaultToNull()) {
                        f.set(val, null);
                     } else {
                        throw ex;
                     }
                  }
                  continue;
               }
            }

            // ConfigurationList mappings
            {
               ConfigurationList annotation = f.getAnnotation(ConfigurationList.class);
               if (null != annotation) {
                  f.setAccessible(true);
                  String subKey = (key + "." + annotation.element()).replace("..", ".");

                  // Set value handling defaulting to null if there is an error
                  try {
                     f.set(val, new CList<>(subKey, annotation.subElementMapping(), f, config, configurationValues));
                  } catch (Exception ex) {
                     if (annotation.defaultToNull()) {
                        f.set(val, null);
                     } else {
                        throw ex;
                     }
                  }
                  continue;
               }
            }

            // ConfigurationStructure mappings
            {
               ConfigurationStructure annotation = f.getAnnotation(ConfigurationStructure.class);
               if (null != annotation) {
                  f.setAccessible(true);
                  String subKey = (key + "." + annotation.element()).replace("..", ".");

                  // Set value handling defaulting to null if there is an error
                  try {
                     f.set(val, new CStructure<>(subKey, f, config, configurationValues));
                  } catch (Exception ex) {
                     if (annotation.defaultToNull()) {
                        f.set(val, null);
                     } else {
                        throw ex;
                     }
                  }
                  continue;
               }
            }

            // Configuration mappings
            {
               Configuration annotation = f.getAnnotation(Configuration.class);
               if (null != annotation) {
                  f.setAccessible(true);
                  String subKey = (key + "." + annotation.element()).replace("..", ".");
                  String defaultValue = DEFAULT.equals(annotation.defaultValue()) ? null : annotation.defaultValue();
                  switch (f.getType().getName()) {
                     case "org.rogueware.configuration.delegate.CInteger":
                        f.set(val, new CInteger(subKey, defaultValue, config, configurationValues));
                        break;
                     case "org.rogueware.configuration.delegate.CLong":
                        f.set(val, new CLong(subKey, defaultValue, config, configurationValues));
                        break;
                     case "org.rogueware.configuration.delegate.CFloat":
                        f.set(val, new CFloat(subKey, defaultValue, config, configurationValues));
                        break;
                     case "org.rogueware.configuration.delegate.CDouble":
                        f.set(val, new CDouble(subKey, defaultValue, config, configurationValues));
                        break;
                     case "org.rogueware.configuration.delegate.CString":
                        f.set(val, new CString(subKey, defaultValue, config, configurationValues));
                        break;
                     case "org.rogueware.configuration.delegate.CBoolean":
                        f.set(val, new CBoolean(subKey, defaultValue, config, configurationValues));
                        break;
                     case "org.rogueware.configuration.delegate.CTimeDuration":
                        f.set(val, new CTimeDuration(subKey, defaultValue, config, configurationValues));
                        break;
                     default:
                        throw new IllegalArgumentException(String.format("Configuration value cannot be injected into field '%s' that is not of type ConfigurationValue", f.getName()));
                  }
               }
            }
         }

         // Add the element to the map
         configurationValues.put(key, val);
      } catch (InstantiationException | IllegalAccessException ex) {
         throw new IllegalArgumentException(String.format("Unable to create structure value for key '%s' with structure class '%s'", key, structureClass.getName()), ex);
      }
   }

   protected <T> T getStoredConfigurationValue() {
      if (!configurationValues.containsKey(getKey())) {
         storeConfigurationValue();
      }
      return (T) configurationValues.get(getKey());
   }

   @Override
   public String getKey() {
      return key;
   }

   @Override
   public String toString() {
      S structure = structure();

      StringBuilder sb = new StringBuilder();
      sb.append(key);
      sb.append("={\n   ");
      sb.append(structure().toString());
      sb.append("}");
      return sb.toString();
   }

   @Override
   public String strValue() {
      return "";
   }

   public S structure() {
      // Return reference to the structure .. then if structure replaced in store, existing execution runs with it until out of scope
      // This should allow using structure to complete before new copy is requested
      S structure = getStoredConfigurationValue();
      return structure;
   }
}
