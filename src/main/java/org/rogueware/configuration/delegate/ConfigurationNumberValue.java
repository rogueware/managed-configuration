/*
 * Copyright 2017 jingram.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.rogueware.configuration.delegate;

import jakarta.enterprise.inject.Vetoed;
import java.util.Map;
import org.apache.commons.configuration.HierarchicalConfiguration;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Vetoed
public abstract class ConfigurationNumberValue extends Number implements ConfigurationValue {

   protected String key;
   protected String defaultValue;
   protected HierarchicalConfiguration config;
   protected Map<String, Object> configurationValues;   // Values will change when new config loaded

   protected ConfigurationNumberValue(String key, String defaultValue, HierarchicalConfiguration config, Map<String, Object> configurationValues) {
      this.key = key;
      this.defaultValue = defaultValue;
      this.config = config;
      this.configurationValues = configurationValues;
   }

   protected abstract void storeConfigurationValue();

   protected <T> T getStoredConfigurationValue() {
      if (!configurationValues.containsKey(getKey())) {
         storeConfigurationValue();
      }
      return (T) configurationValues.get(getKey());
   }

   @Override
   public String getKey() {
      return key;
   }

   @Override
   public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append(getKey());
      sb.append("=");
      sb.append(strValue());
      return sb.toString();
   }
}
