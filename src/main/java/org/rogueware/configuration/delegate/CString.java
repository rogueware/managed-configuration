/*
 * Copyright 2017 jingram.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.rogueware.configuration.delegate;

import jakarta.enterprise.inject.Vetoed;
import java.util.Map;
import org.apache.commons.configuration.HierarchicalConfiguration;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Vetoed
public class CString implements ConfigurationValue, Comparable<String> {

   String key;
   String defaultValue;
   HierarchicalConfiguration config;
   Map<String, Object> configurationValues;   // Values will change when new config loaded

   public CString(String key, String defaultValue, HierarchicalConfiguration config, Map<String, Object> configurationValues) {
      this.key = key;
      this.defaultValue = defaultValue;
      this.config = config;
      this.configurationValues = configurationValues;

      // Store the value on creation (First injection)
      storeConfigurationValue();
   }

   private void storeConfigurationValue() {
      // Store a specific string value
      String val = "";
      String[] vals = config.getStringArray(key);
      if (null != defaultValue) {
         // Use get string array incase the value has a , that needs to be used.
         // Will need to put the string back together again
         if (0 == vals.length) {
            val = defaultValue;
         } else {
            val = String.join(",", vals);
         }
      } else {
         if (0 != vals.length) {
            val = String.join(",", vals);
         }
      }

      configurationValues.put(key, val);
   }

   private <T> T getStoredConfigurationValue() {
      if (!configurationValues.containsKey(getKey())) {
         storeConfigurationValue();
      }
      return (T) configurationValues.get(getKey());
   }

   @Override
   public String getKey() {
      return key;
   }

   @Override
   public final String strValue() {
      return getStoredConfigurationValue();
   }

   @Override
   public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append(getKey());
      sb.append("=");
      sb.append(strValue());
      return sb.toString();
   }

   // Re-implement some of the nice string functions
   // -----------------------------------------------
   @Override
   public boolean equals(Object anObject) {
      return strValue().equals(anObject);
   }

   public boolean equalsIgnoreCase(String anotherString) {
      return strValue().equalsIgnoreCase(anotherString);
   }

   @Override
   public int compareTo(String anotherString) {
      return strValue().compareTo(anotherString);
   }

   public int compareToIgnoreCase(String str) {
      return strValue().compareToIgnoreCase(str);
   }

   public boolean startsWith(String prefix) {
      return strValue().startsWith(prefix);
   }

   public boolean endsWith(String suffix) {
      return strValue().endsWith(suffix);
   }

   public int indexOf(int ch) {
      return strValue().indexOf(ch);
   }

   public int lastIndexOf(int ch) {
      return strValue().lastIndexOf(ch);
   }

   public int indexOf(String str) {
      return strValue().indexOf(str);
   }

   public int lastIndexOf(String str) {
      return strValue().lastIndexOf(str);
   }

   public String substring(int beginIndex) {
      return strValue().substring(beginIndex);
   }

   public String substring(int beginIndex, int endIndex) {
      return strValue().substring(beginIndex, endIndex);
   }

   public boolean contains(CharSequence s) {
      return strValue().contains(s);
   }

   public String toLowerCase() {
      return strValue().toLowerCase();
   }

   public String toUpperCase() {
      return strValue().toUpperCase();
   }

   public String replace(char oldChar, char newChar) {
      return strValue().replace(oldChar, newChar);
   }

   public String replaceFirst(String regex, String replacement) {
      return strValue().replaceFirst(regex, replacement);
   }

   public String replaceAll(String regex, String replacement) {
      return strValue().replaceAll(regex, replacement);
   }

   public String replace(CharSequence target, CharSequence replacement) {
      return strValue().replace(target, replacement);
   }
}
