/*
 * Copyright 2017 jingram.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.rogueware.configuration.delegate;

import jakarta.enterprise.inject.Vetoed;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.rogueware.configuration.Configurator;
import static org.rogueware.configuration.Configurator.DEFAULT;
import org.rogueware.configuration.annotation.ConfigurationAttribute;
import org.rogueware.configuration.qualifier.Configuration;
import org.rogueware.configuration.qualifier.ConfigurationList;
import org.rogueware.configuration.qualifier.ConfigurationMap;
import org.rogueware.configuration.qualifier.ConfigurationStructure;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Vetoed
public class CList<L extends List> implements ConfigurationValue {

   String key;
   String subElement;
   Class listValueClass;

   HierarchicalConfiguration config;
   Map<String, Object> configurationValues;   // Values will change when new config loaded

   protected CList() {
   }

   public CList(String key, String subElement, Field injectedField, HierarchicalConfiguration config, Map<String, Object> configurationValues) {
      // Injected
      this.key = key;
      this.subElement = Configurator.DEFAULT.equals(subElement) ? null : subElement;

      // Determine the list value class for the generic type
      try {
         // Must be a generic type of List
         ParameterizedType pt = (ParameterizedType) injectedField.getGenericType();
         Type listType = pt.getActualTypeArguments()[0];
         String listTypeName = listType.getTypeName();

         String valClassName = listTypeName.replaceFirst("java.util.List<", "").replace(">", "").trim();

         this.listValueClass = Class.forName(valClassName);
      } catch (Exception ex) {
         throw new IllegalArgumentException(String.format("Unable to find or determine List generic type for injected configuration field '%s'", injectedField.getName()), ex);
      }

      
      this.config = config;
      this.configurationValues = configurationValues;

      // Store the value on creation (First injection)
      storeConfigurationValue();
   }

   public CList(String key, String subElement, ParameterizedType pt, HierarchicalConfiguration config, Map<String, Object> configurationValues) {
      // Literal
      this.key = key;
      this.subElement = Configurator.DEFAULT.equals(subElement) ? null : subElement;

      // Determine the list value class for the generic type
      try {
         // Must be a generic type of List
         Type listType = pt.getActualTypeArguments()[0];
         String listTypeName = listType.getTypeName();

         String valClassName = listTypeName.replaceFirst("java.util.List<", "").replace(">", "").trim();

         this.listValueClass = Class.forName(valClassName);
      } catch (Exception ex) {
         throw new IllegalArgumentException("Unable to find or determine List generic type for LITERAL lookup", ex);
      }

      
      this.config = config;
      this.configurationValues = configurationValues;

      // Store the value on creation (First injection)
      storeConfigurationValue();
   }
   
   
   private void storeConfigurationValue() {
      Collection list = Collections.synchronizedList(new ArrayList());

      // If no config, then store an empty list
      if (config.containsKey(key) || null != subElement) {
         if (null == subElement) {
            // Values are in element (Cannot have struct)

            String[] vals;
            if (null == config.getString(key) || config.getString(key).isEmpty()) {
               vals = new String[0];  // Empty
            } else {
               vals = config.getStringArray(key);
            }

            for (String val : vals) {
               list.add(getArrayElementVal(val, listValueClass));
            }
         } else {
            // Populate list from sub elements (Can have a struct)
            List<HierarchicalConfiguration> subElements = config.configurationsAt((key + "." + subElement).replace("..", "."));
            int index = 0;
            for (HierarchicalConfiguration configSubElement : subElements) {
               try {
                  // Create the value object
                  Object val;
                  if (DelegateUtility.BASE_CLASSES.contains(listValueClass)) {
                     // Supported Primitive Wrappers, String & String[]
                     val = DelegateUtility.getConfigurationAttributeValue(configSubElement, null, listValueClass, Configurator.DEFAULT);
                  } else {
                     // Annotated class
                     val = listValueClass.newInstance();

                     for (Field f : DelegateUtility.getAllDeclaredClassFields(listValueClass)) {
                        // Attribute mapping
                        {
                           ConfigurationAttribute annotation = f.getAnnotation(ConfigurationAttribute.class);
                           if (null != annotation) {
                              f.setAccessible(true);
                              
                              f.set(val, DelegateUtility.getConfigurationAttributeValue(configSubElement, annotation.attribute(), f.getType(), annotation.defaultValue()));
                              continue;
                           }
                        }

                        // ConfigurationMap mappings
                        {
                           ConfigurationMap annotation = f.getAnnotation(ConfigurationMap.class);
                           if (null != annotation) {
                              f.setAccessible(true);
                              String subKey = (key + "." + subElement + "(" + index + ")" + annotation.element()).replace("..", ".");

                              // Set value handling defaulting to null if there is an error
                              try {
                                 f.set(val, new CMap<>(subKey, annotation.mapKeyAttribute(), annotation.mapValueAttribute(), f, config, configurationValues));
                              } catch (Exception ex) {
                                 if (annotation.defaultToNull()) {
                                    f.set(val, null);
                                 } else {
                                    throw ex;
                                 }
                              }
                              continue;
                           }
                        }

                        // ConfigurationList mappings
                        {
                           ConfigurationList annotation = f.getAnnotation(ConfigurationList.class);
                           if (null != annotation) {
                              f.setAccessible(true);
//                              String subKey = (key + "(" + index + ")").replace("..", ".");                            
                              String subKey = (key + "." + subElement + "(" + index + ")" + (annotation.element().equals(".") ? "" : ".") + annotation.element()).replace("..", ".");

                              // Set value handling defaulting to null if there is an error
                              try {
                                 f.set(val, new CList<>(subKey, annotation.subElementMapping(), f, config, configurationValues));
                              } catch (Exception ex) {
                                 if (annotation.defaultToNull()) {
                                    f.set(val, null);
                                 } else {
                                    throw ex;
                                 }
                              }
                              continue;
                           }
                        }

                        // ConfigurationStructure mappings
                        {
                           ConfigurationStructure annotation = f.getAnnotation(ConfigurationStructure.class);
                           if (null != annotation) {
                              f.setAccessible(true);
//                              String subKey = (key + "." + subElement + "(" + index + ")" + annotation.element()).replace("..", ".");
                              String subKey = (key + "." + subElement + "(" + index + ")" + (annotation.element().equals(".") ? "" : ".") + annotation.element()).replace("..", ".");

                              // Set value handling defaulting to null if there is an error
                              try {
                                 f.set(val, new CStructure<>(subKey, f, config, configurationValues));
                              } catch (Exception ex) {
                                 if (annotation.defaultToNull()) {
                                    f.set(val, null);
                                 } else {
                                    throw ex;
                                 }
                              }
                              continue;
                           }
                        }

                        // Configuration mappings
                        {
                           Configuration annotation = f.getAnnotation(Configuration.class);
                           if (null != annotation) {
                              f.setAccessible(true);
                              String subKey = (key + "." + subElement + "(" + index + ")" + annotation.element()).replace("..", ".");
                              String defaultValue = DEFAULT.equals(annotation.defaultValue()) ? null : annotation.defaultValue();
                              switch (f.getType().getName()) {
                                 case "org.rogueware.configuration.delegate.CInteger":
                                    f.set(val, new CInteger(subKey, defaultValue, config, configurationValues));
                                    break;
                                 case "org.rogueware.configuration.delegate.CLong":
                                    f.set(val, new CLong(subKey, defaultValue, config, configurationValues));
                                    break;
                                 case "org.rogueware.configuration.delegate.CFloat":
                                    f.set(val, new CFloat(subKey, defaultValue, config, configurationValues));
                                    break;
                                 case "org.rogueware.configuration.delegate.CDouble":
                                    f.set(val, new CDouble(subKey, defaultValue, config, configurationValues));
                                    break;
                                 case "org.rogueware.configuration.delegate.CString":
                                    f.set(val, new CString(subKey, defaultValue, config, configurationValues));
                                    break;
                                 case "org.rogueware.configuration.delegate.CBoolean":
                                    f.set(val, new CBoolean(subKey, defaultValue, config, configurationValues));
                                    break;
                                 case "org.rogueware.configuration.delegate.CTimeDuration":
                                    f.set(val, new CTimeDuration(subKey, defaultValue, config, configurationValues));
                                    break;
                                 default:
                                    throw new IllegalArgumentException(String.format("Configuration value cannot be injected into field '%s' that is not of type ConfigurationValue", f.getName()));
                              }
                           }
                        }
                     } // end for 
                  }

                  // Add the element to the list if its not null
                  if (null != val) {
                     list.add(val);
                  }
               } catch (InstantiationException | IllegalAccessException ex) {
                  throw new IllegalArgumentException(String.format("Unable to create map value for key '%s' with value class '%s'", key, listValueClass.getName()), ex);
               }

               index++;
            }
         }
      }

      configurationValues.put(key, list);
   }

   private <T> T getStoredConfigurationValue() {
      if (!configurationValues.containsKey(getKey())) {
         storeConfigurationValue();
      }
      return (T) configurationValues.get(getKey());
   }

   private Object getArrayElementVal(String val, Class type) {
      if (String.class == type) {
         return val;
      }
      if (Boolean.class == type) {
         return Boolean.parseBoolean(val);
      }
      if (Byte.class == type) {
         return Byte.parseByte(val);
      }
      if (Short.class == type) {
         return Short.parseShort(val);
      }
      if (Integer.class == type) {
         return Integer.parseInt(val);
      }
      if (Long.class == type) {
         return Long.parseLong(val);
      }
      if (Float.class == type) {
         return Float.parseFloat(val);
      }
      if (Double.class == type) {
         return Double.parseDouble(val);
      }
      throw new IllegalArgumentException(String.format("Cannot map list value object field type '%s''", type.getName()));
   }

   @Override
   public String getKey() {
      return key;
   }

   @Override
   public String toString() {
      L list = list();

      StringBuilder sb = new StringBuilder();
      sb.append(key);
      sb.append("=[\n");
      for (Object k : list) {
         sb.append("   ");
         sb.append(k);
         sb.append("\n");
      }
      sb.append("]");
      return sb.toString();
   }

   @Override
   public String strValue() {
      StringBuilder sb = new StringBuilder();
      for (Object val : (L) getStoredConfigurationValue()) {
         if (sb.length() > 0) {
            sb.append(",");
         }
         sb.append(val.toString());
      }
      return sb.toString();
   }

   public L list() {
      // Return reference to list .. then if list replaced in store, existing execution runs with it until out of scope
      // This should allow iteration over list to complete on old list and when requesting next copy, can then run on any new list from config reload
      L list = getStoredConfigurationValue();
      return list;
   }

}
