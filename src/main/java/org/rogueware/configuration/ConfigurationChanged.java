/*
 * Copyright 2017 jingram.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.rogueware.configuration;

import java.util.Date;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class ConfigurationChanged {

   private final String configFile;
   private final Date modified;

   public ConfigurationChanged(String configFile, Date modified) {
      this.configFile = configFile;
      this.modified = modified;
   }

   public String getConfigFile() {
      return configFile;
   }

   public Date getModified() {
      return modified;
   }
}
